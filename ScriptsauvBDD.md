# Création script de sauvegarde d'une base de données de type mysql

## Création d'une base de données
Tout d'abord avant de créer une base de données, il faut disposer d'un outil de création.

Pour cela nous utilisons mysql.
Pour l'installer, il suffit avec le gestionnaire de paquet apt de taper les commandes suivantes :
apt install default-mysql-server
apt install default-mysql-client

L'outil mysqldump qui permet d'effectuer des sauvegardes est contenu dans ces packages.

Une fois installé, nous pouvons commencer la création d'une base de données. Voici une liste de commande utile.

mysql
create database testdb;
create user 'testuser'@'localhost' identified by 'test1234';
grant all on testdb.* to 'testuser' identified by 'test1234';
exit
-> Ici, on crée une base de données "testdb", un utilisateur "testuser" et on lui attribue tous les pouvoirs sur cette base de données.


use testdb
CREATE TABLE latable (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, unmot CHAR(25), unnombre INT(9));
-> On crée une table sur la base de données "testdb"

INSERT INTO latable (id, unmot, unnombre) VALUES (NULL, 'salut' , '5489'); 
INSERT INTO latable (id, unmot, unnombre) VALUES (NULL, 'aurevoir' , '666');
-> On insert des données dans la table "latable" 

pour voir si la table existe bien et si les valeurs sont correctes :
select * from latable;


## Création du script
###Source du fichier .backconf
Pour créer un script, on commence par créer un fichier .sh.
Le notre se nommera sauvBDD.sh

Pour le rendre executable par n'importe quel utilisateur, on peut entrer la commande suivante :
chmod +x sauvBDD.sh

A partir de maintenant, on peut commencer à écrire le script.
Tout d'abord l'objectif était de réaliser une sauvegarde. Pour cela j'ai commencé par écrire le script suivant :
```bash
empsauv=$HOME/sauvbdd
tdate=$(date +%Y%m%d-%H%M%S)
fsauv=$empsauv/sauvbdd$tdate.sql
sauvbdd() {
	mysqldump --user=$1 --password=$2 --databases $3 > $fsauv
	echo -e "user= $1\npassword= $2\nemplacementsauvegarde= $empsauv\ndatabases : " > $HOME/.backupconf
}
sauvbdd $@
```

Ici je commence par initialiser les variables qui me serviront (empsauv est l'emplacement des sauvegardes, 
tdate une variable me permettant de récupérer la date actuelle sous la forme AAAAMMJJ-hhmmss, et ensuite
fsauv qui l'emplacement de la sauvegarde finale du script).
Ensuite je crée une fonction dans laquelle j'utilise la commande suivante :
mysqldump --user=$1 --password=$2 --databases $3 > $fsauv
Avec cette simple commande, je crée une sauvegarde de la base de donnée donné en troisième argument, en utilisant le nom d'utilisateur en premier argument le mdp en deuxième.
Cette sauvegarde aura comme chemin celui de la variable fsauv ($HOME/sauvbdd/sauvbddAAAAMMJJ-hhmmss.sql
Après ceci, j'enregistre les paramètres utilisé dans un fichier .backconf dans le home de l'utilisateur qui aura lancé le script.

Pour le tester on peut faire un cp sauvBDD.sh /bin/sauvBDD.
Ensuite, pour lancer le script, l'utilisateur devra utiliser la commande suivante :
sauvBDD 'nomutilisateur' 'motdepasse' 'nomdelabasededonne'


### Gestion des versions et purge automatique
Pour la purge automatique, j'ai utilisé la fonction find. Elle permet de rechercher n'importe quel type de fichier/repertoire et de les sélectionner suivant leur date de création/modification
j'ai donc ajouté cette fonction au script :
```bash
suppvers() {
find /empsauv/* -atime 7 -exec echo $tdate 'suppression de '{} >> $emplog \; -exec rm {} \;
}
```
-atime pour récupérer la date de création
-mtime pour récupérer la date de modification
7 correspond à 7 jours, ainsi tous les fichiers dont la date de création remonte à 7 jours ou plus seront selectionné.
-exec permet d'effectuer une action sur les fichiers selectionnés.
Ainsi j'ai fait :
- echo %tdate 'suppression de '{} >> $emplog, qui a pour but de savoir quel fichier a été selectionné 
- rm {}, qui supprime le fichier actuellement selectionné

### Gestion des logs
Dans l'objectif de tracer les actions effectués du script, j'ai ajouté la fonction suivante à mon script.
```bash
log() {
	echo $tdate $1 >>$emplog
}
```
Avec cette petite fonction, il suffit de placer un paramètre après l'appelle de la fonction pour ajouter une ligne au fichier emplog correspondant au paramètre.
Exemple d'utilisation :
log "$tdate ajout d'une ligne"

Cependant, pour permettre à tout le monde d'executer le script dans de bonne condition, j'ai du changer les permissions d'accès au fichier de log.
Pour cela j'ai modifier les permissions avec la commande suivante :
chmod g-r,o-r,a+w /var/log/sauvBDD.log
Ainsi le propriétaire pourra toujours écrire et lire le fichier, et les autres utilisateurs pourront écrire sur celui-ci via le script, mais ne pourront pas le lire.


### Gestion erreurs
Pour faire une gestion des erreurs, j'ai créé une fonction qui lit le code retour de la fonction précédemment executé.
La voici :
```bash
testerrBDD() {
coder=$?
if [ "$coder" = 0]; then
log "sauvegarde $1 reussie"
else
log "erreur de sauvegarde sur $1 - Code : $coder"
fi
}
```
Si le code retour est égale à 0, alors la sauvegarde est marqué comme réussi dans les logs, sinon c'est une erreur et son code est envoyé.
J'utilise cette fonction juste après la création de sauvegarde, tel quel :
```bash
mysqldump --user=$user--password=$pw --databases $i > $fsauv
testerrBDD $i  
```

Si une autre commande est ajouté entre les deux, alors ça sera le code retour de cette commande qui sera recupéré, il faut donc faire attention à l'ordre d'execution.



## Sauvegarder de plusieurs base de données
Pour sauvegarder plusieurs base de données, j'ai tout simplement utilisé un "shift 2" sur ma fonction de base.
Ceci me permet de supprimer mes deux premiers arguments (le nom d'utilisateur et le mot de passe), me permettant de lire ainsi de lire chaque argument suivant dans un for.
```bash
sauvbdd2() {
user=$1
pw=$2
echo -e "user= $1\npassword= $2\nemplacementsauvegarde= $empsauv\ndatabases : " > $HOME/.backupconf
shift 2
	for i in "$@"; do
	mysqldump --user=$user--password=$pw --databases $i > $fsauv
	testerrBDD $i
	echo $i >> $HOME/.bachupconf
	done
}
```


## Restauration d'une base à partir d'une sauvegarde
Pour restaurer une base de donnée, il suffit de récupérer le nom d'utilisateur, le mot de passe et le nom d'une base de donnée (qui sont donc disponible dans le fichier .backupconf) et le fichier de sauvegarde correspondant.
Voici ensuite la commande à utiliser
```bash
mysql --user=testuser --password=test1234 testdb < sauvBDDAAAAMMJJ-hhmmss.sql
```





## Code complet et final du script
```bash
emplog=/var/log/sauvbdd.log
empsauv=$HOME/sauvbdd

tdate=$(date +%Y%m%d-%H%M%S)
fsauv=$empsauv/sauvbdd$tdate.sql

log() {
	echo $tdate $1 >>$emplog
}
testemp() {
if [ ! -d $empsauv ]; then
mkdir $empsauv
fi
}
suppvers() {
find /empsauv/* -atime 7 -exec echo $tdate 'suppression de '{} >> $emplog \; -exec rm {} \;
}
testerrBDD() {
coder=$?
if [ "$coder" = 0]; then
log "sauvegarde $1 reussie"
else
log "erreur de sauvegarde sur $1 - Code : $coder"
fi
}
sauvbdd() {
user=$1
pw=$2
echo -e "user= $1\npassword= $2\nemplacementsauvegarde= $empsauv\ndatabases : " > $HOME/.backupconf
shift 2
	for i in "$@"; do
	mysqldump --user=$user--password=$pw --databases $i > $fsauv
	testerrBDD $i
	echo $i >> $HOME/.bachupconf
	done
}

testemp
suppvers
sauvbdd $@
```



# Projet d'intégration

## Présentation du projet
Pour ce projet, j'ai choisi de créer un serveur web linux sur une vm permettant de disposer d'un wiki.

Un wiki est une application web sur lequel les visiteurs peuvent effectuer des modifications, permettant ainsi par collaborations de rassembler une vaste quantité d'informations.
Pour un bon  fonctionnement, il est nécessaire d'identifier les auteurs, de suivre les modifications et d'archiver les versions.

Pour intégrer ce projet, j'ai choisi les technologies suivantes :
-un serveur nginx, permettant de créer mon site web.
-dokuwiki, une application wiki qui dispose de toutes les fonctionnalités nécessaire.

## Installation solution

### Installation nginx
installation de nginx :
```bash
apt install nginx
```

repertoire nginx :
/etc/nginx

Pour la création du site faire :
```bash
cd /etc/nginx/sites-available
rm default
nano monsite
```

Puis écrire ça :
```
server {
	listen 80;
	listen [::]:80;
	server_name monsite.fr;
	root /var/www/monsite;
	index index.php index.html;
    location / {
         try_files $uri $uri/ =404;
     }
}
```

Ensuite il faut ajouter un lien entre dossier sites-available et sites-enabled :
```bash
ln -s /etc/nginx/sites-available/monsite /etc/nginx/sites-enabled/monsite
```

Le dossier sites-available ajoute des sites au serveur et le dossier sites-enabled les actives.
Le lien permet de "synchroniser" les deux fichiers. Ainsi, si on modifie un des deux fichiers, l'autre sera aussi modifié.


création du dossier contenant les pages du site :
```bash
cd /var/www
mkdir monsite
```


création d'une pageweb :
```bash
cd monsite/
nano index.php -> 'test ok'
```

test configuration :
```bash
nginx -t
```

si ok, on relance le nginx :
```bash
service nginx restart
```

Pour tester la page web sur la vm, taper dans un navigateur : localhost:80


Pour tester si la solution est bien installé :
Sur VirtualBox, passer la carte réseau de la vm en Réseau privé hôte, puis vérifier l'adresse ip de la vm.
Pour voir la page web, ouvrir un navigateur et taper l'ip puis le port adéquat.
ex : 192.168.56.101:80

Normalement si, tout marche, on est sensé voir la page web.

A partir de là, nous avons donc installé avec un serveur web avec succés et nous pouvons modifier comme souhaité notre site.


### Installation php-pfm
En vue d'utiliser l'application dokuwiki, il sera nécessaire d'installer php-pfm, une interface pour communiquer entre notre serveur nginx et php, et donc l'appli dokuwiki.

Pour l'installer :
```bash
apt-get install php-fpm
```


liaison avec nginx :
```bash
cd /etc/php/(numversion)/fpm/pool.d
nano www.conf
```
puis modifier la ligne listen avec
listen = 127.0.0.1:9000


ensuite on va faire :
```bash
nano /etc/nginx/sites-available/monwiki
```
puis on va ajouter ceci au fichier
```
location ~ \.php$ {
        try_files $uri =404;
         fastcgi_index index.php;
         fastcgi_pass 127.0.0.1:9000;
         fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
         include /etc/nginx/fastcgi_params;
}
```
 
Ceci permet de modifier le fichier de configuration de nginx pour le faire interpréter tous les fichiers php avec le module php-fpm en se connectant au port 9000.
 
 
Après ça, on va modifier le fichier php de notre site :
```bash
nano /var/www/monwiki
```
avec le code suivant
```
<?php
phpinfo();
?>
```

Ce code permet de tester le bon fonctionnement du module php-fpm.

Finalement, pour tester ceci, on va d'abord redémarrer les services php-fpm et nginx avec les commandes :
```bash
service php7.3-fpm restart
service nginx restart
```

Puis on essaie de se connecter à notre page. Si on voit bien les informations de notre version php sur notrepage, alors c'est que tout fonctionne bien.



### Installation Dokuwiki
Pour commencer, il faut récupérer les paquets via la commande :
```bash
apt-get install dokuwiki
```

Ensuite il faut modifier le fichier de conf de notre site pour qu'il puisse se lier avec dokuwiki.
Pour cela, copier ça :
```
server {
        listen 80;
        listen [::]:80;
  server_name LWiki.fr;
  root /var/www/LWiki;

  location / {
    index index.php;
    try_files $uri $uri/ @dokuwiki;
  }

  location ~ ^/lib.*\.(gif|png|ico|jpg)$ {
    expires 30d;
  }

  location ^~ /conf/ { return 403; }
  location ^~ /data/ { return 403; }

  location @dokuwiki {
    rewrite ^/_media/(.*) /lib/exe/fetch.php?media=$1 last;
    rewrite ^/_detail/(.*) /lib/exe/detail.php?media=$1 last;
    rewrite ^/_export/([^/]+)/(.*) /doku.php?do=export_$1&id=$2 last;
    rewrite ^/(.*) /doku.php?id=$1 last;
  }

  location ~ \.php$ {
    include fastcgi_params;
    fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    fastcgi_pass 127.0.0.1:9000;
  }
}
```


Ensuite télécharger la page de base pour dokuwiki :
```bash
wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz 
```

Extraire le paquet avec :
```bash
tar xzvf dokuwiki-stable.tgz
```

Copier le contenu dans la page de votre site (pour moi /var/www/LWiki/)
```bash
cp -r . /var/www/LWiki/
```

Puis modifier le propriétaire du site :
```bash
chown -R www-date:www-data /var/www/LWiki/
```


A partir de là, votre site contient dokuwiki et est configurable.
Pour le paraméter, entre l'ip du site plus /install.php 

## Configuration sauvegarde

### Partitionnement
Pour permettre une sauvegarde et restauration simplifié, j'ai commencé par créer une partition qui contiendra les données sensibles.
De plus, avoir une partition avec les données séparées du reste du système permet de garder un système opérationnel si les données venaient à prendre trop de place.

Disposant de 2G d'espace libre sur mon disque, j'ai commencé par observer les partitions présentes
```bash
fdisk -l pour voir les différents disques et partition déjà présente
```

Pour lancer l'utilitaire de gestion du partitionnement sur le disque sda
```bash
fdisk /dev/sda
```
F pour voir l'espace libre
p pour afficher les tables de partitions
n pour ajouter une nouvelle partition ->
choisir p pour primaire
choisir numéro de partition (choisir le num par défaut)
ensuite il faut choisir le premier secteur, dans mon cas je prend comme premier secteur le secteur qui se trouve juste après la dernière partition.
37 421 056
Il faut bien faire attention aux secteurs déjà utilisé, que l'on peut voir avec la commande p utilisé précédemment. 
Après cela pour choisir la taille de la nouvelle partition, on peut soit directement entre la taille souhaiter, soit on peut calculer la différence entre le premier secteur et le dernier secteur.
Dans mon cas je prend toute la place disponible, donc je prend dernier secteur possible (41 516 479).
Pour écrire la table sur le disque , faire w.

Une fois la partition crée, on l'a formate en ext4 pour la rendre accessible.
```bash
mkfs.ext4 /dev/partition
```

A partir de là, en faisant la commande suivante, notre partition est sensé être visible.
```bash
df -h
```

Après cela, il nous reste qu'à deplacer les données sur notre partition.
Pour effectuer cette manipulation, il sera necessaire passer en mode recovery, nous permettant d'effectuer des modifications sur la partition /.
Pour cela, utiliser la commande :
```bash
sudo telinit 1
```

Une fois rentré en mode de récupération, commencer par monter la partition en mode read&write.
```bash
mount -o rw,remount /
```

Ensuite, on va monter notre partition avec un dossier temporaire:
```bash
mkdir /tempm
mount /dev/sda3 /tempm/
```

On peut vérifier si notre partition a bien été monté :
```bash
df -h
```

On va ensuite copié le contenu de nos données à déplacer dans notre dossier temporaire. Pour mon projet, je ne déplacer pour l'instant que le dossier /var/www
```bash
cp -a /var/www/. /tempm/
```

On vérifie ensuite que tout a été bien copié :
```bash
ls -la /tempm/
```

Arrivé ici, on va récupérer l'uuid de notre partition pour editer le fichier fstab (ce fichier permet de configurer les points de montage automatique). Pour cela, on utilise la commande suivante :
```bash
blkid /dev/sda3 >> /etc/fstab
```

Après ça, on modifie le fichier /etc/fstab pour ajouter cette ligne :
UUID=(uuiddenotrepartition)      /var/www     ext4        defaults        0       2

On fait ensuite un déplacement et une sauvegarde du dossier /var/www de la partition principale "/".
```bash
mv /var/www /var/www.bak
mkdir /var/www
```

Pour tester il nous reste qu'à reboot et de retaper la commande:
```bash
df -h
```

Normalement notre partition (ici /dev/sda3) doit apparaitre comme étant monté sur /var/www


### synchronisation donnée
Pour ce projet, j'ai choisi de faire comme sauvegarde une simple synchronisation entre ma partition de données et un autre disque, dans le but d'étudier l'outil rsync.

J'ai pour commencer ajouter un disque que j'ai monté sur /sauvdon

Ensuite, en essayant diverses options j'ai choisi d'utiliser la commande :
```bash
rsync -av /var/www /sauvdon/var/ >> /var/log/synchrosauvsite.log
```
L'option a est un raccourci pour -rlptgoD, des options souvent utilisé :
-r pour une copie récursive (dossiers)
-l copie les liens symboliques.
-p conserve les permissions.
-t conserve les date de modifications (nécessaire au bon fonctionnement de l'algorythme de différence).
-g conserve le groupe auquel appartient le fichier.
-o conserve le propriétaire auquel appartient le fichier.
-D préserve les fichiers spéciaux et les fichier devices.
L'option v permet d'obtenir plus de détails en sortie.
De plus, je sauvegarde les sortie de la synchronisation dans le fichier synchrosauvsite.log, me permettant de savoir si tel sauvegarde a bien été effectué avec un script.

J'ai bien écrit /var/www et pas /var/www/ pour récupérer le dossier www et y inclure les fichiers 



### Mise en place d'un script de sauvegarde automatique
Pour la mise en place, j'ai tout simplement écrit le petit script suivant :
```bash
emplog=/var/log/synchrosauvsite.log
echo $(date +%Y%m%d-%H%M%S)>> $emplog
rsync -av /var/www /sauvdon/var/ >> $emplog
echo $? >> $emplog
```

Que j'ai ensuite ajouté dans un crontab. Pour cela j'ai utilisé la commande suivante pour ouvrir crontab :
```bash
crontab -e
```
puis 1 pour sélectionner nano

Ensuite j'ai écrit
0 8 * * * /(emplacementscript)
Ce qui permet de lancer le script tous les jours à 8 heures

A noter que je ne met pas en place de sauvegarde automatique des fichiers de conf étant donné qu'il suffit de les enregistrer une fois et qu'ils ne seront normalement plus modifié plus tard.



### Restauration

Pour une restauration, si c'est le système qu'il faut restaurer :
Il suffit de réinstaller une debian (si nécessaire), de réinstaller les outils utilisé (nginx, php-fpm, dokuwiki) puis de remplacer les fichiers de config.
Pour cela, il faudrait donc prévoir un autre disque avec tous ce qu'il faut d'installer.

S'il faut restaurer les données, il suffit de monter la partition de sauvegarde à la place de celle qui est corrompue, puis de configurer un nouveau disque de sauvegarde.






,
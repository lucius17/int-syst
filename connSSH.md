# Connexion sur machine avec SSH

## Parametrage pour une VM
Sur VirtualBox , créer une machine avec 2 interfaces réseaux.
Une en "réseau privé hôte" pour connecter votre pc à votre vm.
une en "NAT" pour pouvoir connecter la vm à internet.

Sur linux, si la vm ne connecte pas automatiquement la deuxième interface, aller dans /etc/network/interface.
Pour une connexion basique, ajouté à la fin du fichier :
auto "nominterface" (permet de configurer automatiquement l'interface)
allow-hotplug "nominterface"
iface "nominterface" inet dhcp

Pour relancé le service réseau
```bash
service networking restart
```

Pour voir les interfaces et les configurations.
```bash
ip a
```


## Création de clé avec OpenSSH
Si pas installé
```bash
apt-get install openssh-client
```

pour générer clé
```bash
ssh-keygen-t rsa
```

pour se connecter
```bash
ssh utilisateur@ipmachine
```

pour partager sa clé publique, pour ne plus avoir besoin de se connecter avec un mot de passe
```bash
ssh-copy-id -i ~/.ssh/id_rsa.pub user@192.168.56.102
```

bloquer l'usage de mot de passe pour se connecter (pour se prémunir des attaques par force brute)
Dans /etc/ssh/sshd_config
```
PasswordAuthentication no
```

pour bloquer root
```
PermitRootLogin no
```



## Connexion avec putty sur windows
Dans connection -> Data, spécifier dans "Auto-login username" un nom d'utilisateur pour la connexion.

Dans connection -> SSH -> Auth, rechercher un fichier contenant votre clé privé.
Pour une clé crée par PuTTy, il s'agit d'un fichier .ppk
Pour une clé crée par OpenSSH, le fichier n'a de base aucune extension et se nomme "id_rsa" (s'il s'agit d'une clé RSA)
Ne pas oublier de désactiver le filtrage d'extension si on recherche un fichier non .ppk

Dans Session, remplir Host Name (or IP address)
Ensuite choisir un nom de session à sauvegarder puis la sauvegarder. Ceci permettra de ne pas rechercher sa clé et l'ip à chaque reconnexion.

Il est possible de créer un fichier .bat pour directement se connecter à l'aide d'un double-clique
```bash
start /D "C:\Program Files\PuTTy\" putty.exe -ssh 192.168.56.102 -load "nomsession"
```
